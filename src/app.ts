import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as cors from 'cors';
import { configureRouter, initServices } from './configure';
import { addStartTime, expressMetricsMiddleware } from './middlewares/express-metrics.middleware';
import * as uuid from 'uuid/v4';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from './utils/logger';
import { getDatabaseConnectionUrl } from './helpers/connection.helper';
import * as mongoose from 'mongoose';
import { initContextMiddleware } from './middlewares/init-context.middleware';
import AMQP_MANAGER from './amqp/amqp.manager';

configureLogger('mainApp', defaultWinstonLoggerOptions);

export class App {
  private readonly _uuid: string;
  private routesExport = false;
  private privilegesExport = false;
  private dbConnected = false;

  constructor(params: any) {
    this._expressApp = express();

    this.appName = params.appName;

    if (params.configuration) {
      if (this.appName in params.configuration) {
        this.configuration = params.configuration[this.appName];
      }
    }

    this._expressApp.set('port', params.port || process.env.PORT || 3001);
    this._expressApp.set('env', params.env || process.env.NODE_ENV || 'development');
    this._uuid = uuid();
  }

  private _appName: string;

  get appName(): string {
    return this._appName;
  }

  set appName(value: string) {
    this._appName = value;
  }

  private _isReady = false;

  get isReady(): boolean {
    return this._isReady;
  }

  private _listenerOnReady: () => void;

  set listenerOnReady(value: () => void) {
    this._listenerOnReady = value;
  }

  private _dbConnection: any;

  get dbConnection(): any {
    return this._dbConnection;
  }

  set dbConnection(value: any) {
    this._dbConnection = value;
  }

  private _token: string;

  get token(): string {
    return this._token;
  }

  get uuid(): string {
    return this._uuid;
  }

  private _expressApp: express.Application;

  get expressApp(): express.Application {
    return this._expressApp;
  }

  set expressApp(value: express.Application) {
    this._expressApp = value;
  }

  private _port: number;

  get port(): number {
    return this._port;
  }

  set port(value: number) {
    this._port = value;
  }

  private _configuration?: { [key: string]: any };

  get configuration(): { [p: string]: any } {
    return this._configuration;
  }

  set configuration(value: { [p: string]: any }) {
    this._configuration = value;
  }

  async registerAppRouters() {
    const appRouters: express.Router[] = configureRouter(this.configuration);
    // Mount public router to /
    this.expressApp.use('/', appRouters[0]);

    if (appRouters[1]) {
      // Mount private router to /_appName
      this.expressApp.use(`/_${this.appName}`, appRouters[1]);
    }
  }

  applyExpressMiddlewaresRouter(): void {
    this.expressApp.set('trust proxy', 1);
    this.expressApp.use(initContextMiddleware);
    this.expressApp.use(addStartTime);
    this.expressApp.use(bodyParser.json());
    this.expressApp.use(bodyParser.urlencoded({ extended: true }));
    this.expressApp.use(helmet());
    this.expressApp.use(cors());
    this.expressApp.use(expressMetricsMiddleware);
  }

  async bootstrap() {
    await this.previousBootstrap();
    this.applyExpressMiddlewaresRouter();
    await this.registerAppRouters();
    await this.nextBootstrap();
  }

  private setIsReady(value: boolean) {
    this._isReady = value;
    this._listenerOnReady();
  }

  private async nextBootstrap() {
    try {
      await AMQP_MANAGER.connect();
      this.setIsReady(this.dbConnected && AMQP_MANAGER.ready);
      if (this._isReady) {
        initServices();
      }
    } catch (err) {
      const retryTime = 30;
      getLogger('mainApp').log('error', 'Can not initiate authorization correctly, retrying in %d secs', retryTime);
      getLogger('mainApp').log('error', err);
      setTimeout(this.nextBootstrap.bind(this), retryTime * 1000);
    }
  }

  private async previousBootstrap() {
    try {
      if (!this.dbConnected) {
        const databaseUrl = getDatabaseConnectionUrl();
        const mongooseOptions: any = { useNewUrlParser: true, promiseLibrary: global.Promise };
        if (this.configuration.databases.length > 1) {
          mongooseOptions.replicaSet = 'rs0';
        }
        if (databaseUrl) {
          const mongooseObj: any = await mongoose.connect(databaseUrl, mongooseOptions);
          this.dbConnection = mongooseObj.connections[0]; // default conn

          this.dbConnection.on('disconnected', () => {
            getLogger('mainApp').log('warn', 'Database has been disconnected from the micro-service');
          });

          this.dbConnection.on('connected', () => {
            getLogger('mainApp').log(
              'info',
              'Connection on database ready state is ' + this.dbConnection.states[this.dbConnection.readyState]
            );
          });
        } else {
          getLogger('mainApp').log('warn', 'The database url is not configured.');
        }
        this.dbConnected = true;
      }

      this.setIsReady(this.routesExport && this.privilegesExport && this.dbConnected);
    } catch (err) {
      const retryTime = 10;
      getLogger('mainApp').log('error', 'Can not initiate db correctly, retrying in %d secs', retryTime);
      getLogger('mainApp').log('error', err);
      setTimeout(this.previousBootstrap.bind(this), retryTime * 1000);
    }
  }
}
