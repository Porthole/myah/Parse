import * as express from 'express';
import HealthRoutes from './utils/health.routes';
import { AuctionatorService } from './auctionator/auctionator.service';
import { ItemService } from './item/item.service';

/**
 * Function used to configure application
 *
 * @export
 * @param {object} [configuration]
 * @returns {Promise<express.Router[]>}
 */
export function configureRouter(configuration?: object): express.Router[] {
  // Create a new router that will be exported and used by top-level app
  const router = express.Router();
  const routes = [HealthRoutes];

  routes.forEach(r => router.use('/api', r));

  // Create a private router
  const privateRouter = express.Router();

  // Your app-router is now configured, let's export it !
  return [router, privateRouter];
}

export function initServices() {
  [AuctionatorService, ItemService].forEach(s => s.get());
}
