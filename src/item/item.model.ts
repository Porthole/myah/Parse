import { Schema } from 'mongoose';

export const ItemSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  fixedPrice: {
    type: Number
  },
  createdAt: { type: Date, default: Date.now, required: true }
});
