import { Service } from '../utils/service.abstract';
import { IAuctionator } from '../auctionator/auctionator.document';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { ItemSchema } from './item.model';
import { IItem, ItemDocument } from './item.document';
import { getConfiguration } from '../helpers/configuration.helper';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { AuctionatorService } from '../auctionator/auctionator.service';
import ObjectId = mongoose.Types.ObjectId;

const config: any = getConfiguration().user;

export class ItemService extends Service<ItemDocument> {
  constructor(model: Model<ItemDocument>) {
    super(model, 'item');
  }

  public static get(): ItemService {
    return super.getService(ItemSchema, 'item', 'items', ItemService);
  }

  create(itemData: IItem): any {
    const item = new this._model(itemData);
    return item.save();
  }

  async getAll(criteria: any = {}, skip = 0, limit = config.paging.defaultValue) {
    return this._model
      .find(criteria)
      .skip(skip)
      .limit(limit);
  }

  async get(id?: ObjectId, criteria: any = {}) {
    if (id) {
      criteria._id = id;
    }
    const item = await this._model.findOne(criteria);
    if (!item) {
      return item;
    }

    const imports = (
      await AuctionatorService.get()
        .model()
        .find(
          {
            data: { $elemMatch: { name: item.name } }
          },
          {
            data: { $elemMatch: { name: item.name } },
            realm: true,
            faction: true,
            userId: true,
            createdAt: true
          }
        )
        .sort({ createdAt: 1 })
    )
      .map(i => {
        // flat data
        return { ...i._doc, ...i.data[0] };
      })
      .map(i => {
        // cleaning
        delete i.data;
        delete i.name;
        delete i.itemId;
        return i;
      });

    // can't use spread operator due to mongoose data
    return {
      ...JSON.parse(JSON.stringify(item)),
      imports
    };
  }

  async update(id: ObjectId, itemData: any) {
    const item = await this._model.findById(id);
    if (!item) {
      throw new CustomError(CustomErrorCode.ERRNOTFOUND, 'Item not found');
    }

    item.set(itemData);
    return await item.save();
  }

  async checkItems(auctionator: IAuctionator) {
    for (let i = 0; i < auctionator.data.length; i++) {
      let item = await this._model.findOne({ name: auctionator.data[i].name });
      if (!item) {
        item = new this._model({
          name: auctionator.data[i].name,
          createdAt: new Date()
        });
        await item.save();
      }
      auctionator.data[i].itemId = item._id.toString();
    }

    return auctionator;
  }
}
