import AMQP_MANAGER, { CustomQueue } from '../amqp/amqp.manager';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { getConfiguration } from '../helpers/configuration.helper';
import { getPackageName } from '../helpers/package.helper';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from '../utils/logger';
import { ConsumeMessage } from 'amqplib';
import { AuctionatorService } from './auctionator.service';
import * as luaParser from 'luaparse';

const config: any = getConfiguration()[getPackageName()];
const loggerName = 'AuctionatorController';
configureLogger(loggerName, defaultWinstonLoggerOptions);
const logger = getLogger(loggerName);

export class AuctionatorController {
  queueParse: CustomQueue;
  queueResult: CustomQueue;

  initListening() {
    AMQP_MANAGER.createQueue(config.queueParse)
      .then(queue => {
        if (queue) {
          this.queueParse = queue;
          this.queueParse.channel.consume(config.queueParse, msg => this.parseFile(msg));
          return AMQP_MANAGER.createQueue(config.queueResult);
        }
        throw new CustomError(CustomErrorCode.ERRBADREQUEST, 'Queue ' + config.queueParse + ' can not been listened');
      })
      .then(queue => {
        if (queue) {
          this.queueResult = queue;
          return;
        }
        throw new CustomError(CustomErrorCode.ERRBADREQUEST, 'Queue ' + config.queueResult + ' can not been listened');
      })
      .catch(err => {
        logger.log('debug', JSON.stringify(err));
        logger.log('error', 'Can not initiate queues ');
        setTimeout(this.initListening, 30);
      });
  }

  parseFile(msg: ConsumeMessage) {
    const jsonMsg = JSON.parse(msg.content.toString());
    const luaFile = Buffer.from(jsonMsg.data).toString();
    const userId = jsonMsg.userId;
    const parsed: any = luaParser.parse(luaFile);
    AuctionatorService.get()
      .parse(parsed, userId)
      .then(auctionator => {
        return AuctionatorService.get().create(auctionator);
      })
      .then(auctionator => {
        this.queueResult.sendMessage(
          Buffer.from(
            JSON.stringify({
              auctionator,
              userId,
              success: true
            }),
            'utf8'
          )
        );
      })
      .catch(err => {
        logger.log('error', JSON.stringify(err));
        this.queueResult.sendMessage(Buffer.from(JSON.stringify({ err, userId, success: false })));
      });

    this.queueParse.channel.ack(msg);
  }
}

const AUCTIONATOR_CONTROLLER = new AuctionatorController();
export default AUCTIONATOR_CONTROLLER;
