import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { AuctionatorDocument, IAuctionator, ItemImport } from './auctionator.document';
import { Service } from '../utils/service.abstract';
import { AuctionatorSchema } from './auctionator.model';
import { getConfiguration } from '../helpers/configuration.helper';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from '../utils/logger';
import { ItemService } from '../item/item.service';
import { CustomQueue } from '../amqp/amqp.manager';
import { getPackageName } from '../helpers/package.helper';
import ObjectId = mongoose.Types.ObjectId;

const config: any = getConfiguration()[getPackageName()];
const loggerName = 'AuctionatorService';
configureLogger(loggerName, defaultWinstonLoggerOptions);
const logger = getLogger(loggerName);

export class AuctionatorService extends Service<AuctionatorDocument> {
  static queue: CustomQueue;

  constructor(model: Model<AuctionatorDocument>) {
    super(model, 'auctionator');
  }

  public static get(): AuctionatorService {
    return super.getService(AuctionatorSchema, 'auctionator', 'auctionators', AuctionatorService);
  }

  async getAll(criteria: any = {}, skip = 0, limit = config.paging.defaultValue) {
    return this._model
      .find(criteria, { data: false })
      .skip(skip)
      .sort({ createdAt: -1 })
      .limit(limit);
  }

  async get(id?: ObjectId, criteria: any = {}) {
    if (id) {
      criteria._id = id;
    }
    return this._model.findOne(criteria);
  }

  async create(auctionatorData: IAuctionator) {
    const auctionator = new this._model(auctionatorData);
    const alreadyAuctionatorUp = await this._model.findOne({
      realm: auctionatorData.realm,
      createdAt: auctionatorData.createdAt,
      faction: auctionatorData.faction,
      userId: auctionatorData.userId
    });
    if (alreadyAuctionatorUp) {
      throw new CustomError(CustomErrorCode.ERRBADREQUEST, 'Already uploaded file');
    }
    if (!auctionator.data.length) {
      throw new CustomError(CustomErrorCode.ERRBADREQUEST, 'No new data detected. File has not been saved');
    }

    return auctionator.save();
  }

  async parse(parsed: any, uploaderId: ObjectId): Promise<IAuctionator> {
    function itemExist(item: ItemImport, itemsByAuctionator: any): boolean {
      const testItem = itemsByAuctionator[item.itemId.toHexString()];
      if (!testItem) {
        return false;
      }
      for (let i = 0; i < testItem.imports.length; i++) {
        const testImport = testItem.imports[i];
        if (testImport.idImport === item.idImport && testImport.price === item.price) {
          return true;
        }
      }
      return false;
    }

    const items = await ItemService.get()
      .model()
      .find();

    let dateAfter = null;

    for (let i = 0; i < parsed.body.length; i++) {
      const key = parsed.body[i].variables[0].name;
      if (key === 'AUCTIONATOR_DB_MAXHIST_DAYS') {
        dateAfter = new Date();
        dateAfter.setDate(dateAfter.getDate() - parsed.body[i].init[0].value);
      }
    }

    if (!dateAfter) {
      // default 5 last days
      dateAfter = new Date();
      dateAfter.setDate(dateAfter.getDate() - 5);
    }

    const oldAuctionators = await this._model.find({
      userId: uploaderId,
      createdAt: { $gte: dateAfter }
    });
    logger.log('info', 'Loading last ' + oldAuctionators.length + ' imports');

    const itemsByAuctionator: any = {};
    for (let i = 0; i < oldAuctionators.length; i++) {
      for (let j = 0; j < oldAuctionators[i].data.length; j++) {
        const item = oldAuctionators[i].data[j];
        if (!itemsByAuctionator[item.itemId.toHexString()]) {
          itemsByAuctionator[item.itemId.toHexString()] = {
            ...item,
            imports: []
          };
        }
        itemsByAuctionator[item.itemId.toHexString()].imports.push({ idImport: item.idImport, price: item.price });
      }
    }

    const auctionator: IAuctionator = {
      realm: null,
      faction: null,
      data: [],
      userId: uploaderId,
      createdAt: null
    };
    logger.log('info', 'Starting parsing data...');
    for (let i = 0; i < parsed.body.length; i++) {
      const key = parsed.body[i].variables[0].name;
      if (key === 'AUCTIONATOR_PRICE_DATABASE') {
        const data = parsed.body[i].init[0].fields;
        const splitName = data[1].key.raw.replace(/"/gim, '').split('_');
        // get info about the real and faction
        auctionator.realm = splitName[0];
        auctionator.faction = splitName[1];
        logger.log('info', 'Start checking ' + data[1].value.fields.length + ' items');
        for (let j = 0; j < data[1].value.fields.length; j++) {
          let newItem = false;
          // get info about item
          const luaItem = data[1].value.fields[j];
          const item: ItemImport = {
            name: luaItem.key.raw.replace(/"/gim, '')
          };
          let dbItem = items.find(i => i.name === item.name);
          if (!dbItem) {
            dbItem = new (ItemService.get().model())({
              name: item.name,
              createdAt: new Date()
            });
            newItem = true;
            await dbItem.save();
          }
          item.itemId = dbItem._id;
          let mr = null;
          // first step searching "mr" ...
          for (let k = 0; k < luaItem.value.fields.length; k++) {
            // get info about prices saved
            const field = luaItem.value.fields[k];
            const fieldName = field.key.raw.replace(/"/gim, '');
            if (fieldName === 'mr') {
              mr = field.value.value;
            }
          }
          if (!mr) {
            continue;
          }

          for (let k = 0; k < luaItem.value.fields.length; k++) {
            // get info about prices saved
            const field = luaItem.value.fields[k];
            const fieldName = field.key.raw.replace(/"/gim, '');
            let lastIdMatch = 0;

            if (fieldName.match(/[A-Z][0-9]+/gim) && field.value.value === mr) {
              const currentId = parseInt(fieldName.substr(1), 10);
              if (currentId > lastIdMatch) {
                lastIdMatch = currentId;
                item.idImport = fieldName;
                item.price = field.value.value;
              }
            }
          }
          if (item.price) {
            if (newItem) {
              // if new item no need to check old import (optimize this please...)
              auctionator.data.push(item);
            } else if (!newItem && !itemExist(item, oldAuctionators)) {
              auctionator.data.push(item);
            }
          }
        }
      } else if (key === 'AUCTIONATOR_LAST_SCAN_TIME') {
        auctionator.createdAt = new Date(parsed.body[i].init[0].value * 1000);
      }
    }
    logger.log('info', 'End of data parse : Import : ' + auctionator.realm + ' - ' + auctionator.faction);
    return auctionator;
  }
}
