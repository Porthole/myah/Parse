import * as amqp from 'amqplib';
import { Channel, Connection, Options, Replies } from 'amqplib';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from '../utils/logger';
import { getPackageName } from '../helpers/package.helper';
import { getConfiguration } from '../helpers/configuration.helper';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import AUCTIONATOR_CONTROLLER from '../auctionator/auctionator.controller';
import Publish = Options.Publish;
import Consume = Options.Consume;

const appName = getPackageName();

const config: any = getConfiguration()[appName];

export interface IConfigAmqp {
  url: string;
  port: number;
  user: string;
  password: string;
  options: any;
}

export class CustomQueue {
  channel?: Channel;
  queue?: Replies.AssertQueue;

  sendMessage(content: Buffer, options?: Publish) {
    return this.channel.sendToQueue(this.queue.queue, content, options);
  }

  consumeMessage(options?: Consume): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.channel.consume(this.queue.queue, msg => resolve(msg), options);
      } catch (e) {
        reject(e);
      }
    });
  }
}

configureLogger('amqpManager', defaultWinstonLoggerOptions);
const logger = getLogger('amqpManager');

export class AmqpManager {
  private _connection: Connection;
  connected: boolean;
  private _channel: Channel;
  configRabbit: any;
  private _queues = new Map<string, CustomQueue>();

  constructor(config: IConfigAmqp) {
    logger.log('info', 'Initializing Rabbitmq connection');
    this.configRabbit = config;
  }

  get ready() {
    return this.connected;
  }

  get queues(): Map<string, CustomQueue> {
    return this._queues;
  }

  private get channel(): Promise<Channel> {
    if (this._channel) {
      return Promise.resolve(this._channel);
    }
    return this._connection.createChannel().then(channel => {
      this._channel = channel;
      return channel;
    });
  }

  connect() {
    this.connected = false;
    const uri =
      `amqp://${process.env.AMQP_USER}:${process.env.AMQP_PASSWORD}` +
      `@${process.env.AMQP_HOST}:${process.env.AMQP_PORT}`;
    logger.log('info', 'Trying to connect to Rabbitmq');
    return amqp
      .connect(uri)
      .then(connection => {
        this._connection = connection;
        logger.log('info', 'Successfully connected to Rabbitmq ' + this.configRabbit.url);
        this.connected = true;
        this.restartOnFail();
      })
      .catch(err => {
        logger.log('error', err.message);
        this.connected = false;
        setTimeout(() => this.connect(), this.configRabbit.options.reconnectTime);
      });
  }

  private restartOnFail(): void {
    AUCTIONATOR_CONTROLLER.initListening();
    if (!this._connection) {
      logger.log('error', 'Can not instantiate restart on fail');
      return;
    }
    this._connection.on('error', (err: Error) => {
      logger.log('error', err.message);
      this.connected = false;
    });
    this._connection.on('close', () => {
      logger.log('error', 'AmqpConsumer : Reconnecting...');
      this.connect();
    });
  }

  createQueue(queueName: string): Promise<CustomQueue> {
    const currentQueue: CustomQueue = new CustomQueue();
    logger.log('info', 'Create queue on ' + queueName);
    if (this._connection) {
      if (this._queues.get(queueName)) {
        return Promise.resolve(this._queues.get(queueName));
      }
      return this.channel
        .then(channel => {
          currentQueue.channel = channel;
          return channel.assertQueue(queueName, {
            durable: true
          });
        })
        .then(queue => {
          currentQueue.queue = queue;
          this._queues.set(queueName, currentQueue);
          return currentQueue;
        })
        .catch(err => {
          logger.log('error', err.message);
          return Promise.resolve(null);
        });
    }
    logger.log('warn', 'Connection to Rabbitmq not ready yet');
    throw new CustomError(CustomErrorCode.ERRBADREQUEST, 'You have to initiate connection before');
  }
}

const AMQP_MANAGER = new AmqpManager(config.rabbitmq);
export default AMQP_MANAGER;
