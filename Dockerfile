FROM node:10

WORKDIR /opt/parse
COPY . .
RUN npm install
RUN npm run build

CMD npm run start
